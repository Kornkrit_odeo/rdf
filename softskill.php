<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="index.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php
include(dirname(__FILE__).'/db.php');
$uri = isset($_GET['uri']) ? base64_decode($_GET['uri']):NULL ;
$data_career = array();

/* Skill */
$data_skill = array();
$data_skill = DB::query_base('has_skillID','has_skillName','http://www.hozo.jp/owl/ICTCareer.owl#ICTSkillSFIA');

/* softSkill */
$data_softskill = array();
$data_softskill = DB::query_base('has_softskillID','has_SoftSkillName','http://www.hozo.jp/owl/ICTCareer.owl#SoftSkill');

/* training */
$data_training = array();
$data_training = DB::query_base('has_ICTCertID','has_ICTCertName','http://www.hozo.jp/owl/ICTCareer.owl#Training');

/* Education */
$data_education = array();
$data_education = DB::query_base('has_ICTEducationID','has_ICTFieldEducation','http://www.hozo.jp/owl/ICTCareer.owl#ICTEducation');

/* Career */
$data_career = array();
$data_career = DB::query_base('has_ICTCareerID','has_CareerName','http://www.hozo.jp/owl/ICTCareer.owl#ICTCareerISCO');

/* Career */
$result_career = array();
DB::query_prop($result_career,array(
	'type'			=>'softskill',
	'name'			=>'has_CareerName',
	'id'			=>'has_ICTSoftskill',
	'domain'		=>'http://www.hozo.jp/owl/ICTCareer.owl#ICTCareerISCO',
	'uri'			=>$uri
));

$top_name = 'ICT Career';
$skill_name = 'Skill';
$softskill_name = 'Soft Skill';
$education_name = 'Eduction';
$training_name= 'Traning';

$tree_data[0] = array(
	'name'=> $data_softskill[$uri]['name'],
	'parent'=>NULL,
	'children'=>array()
);

if(!empty($result_career))
{
	foreach($result_career as $item)
	{
		$tree_data[0]['children'][] = array(
			'name'=> $item['name'],
			'parent'=>NULL
		);
	}
}

?>

<script src="lib/d3.v3.min.js"></script>
<script>

var treeData = <?php echo json_encode($tree_data);?>;


// ************** Generate the tree diagram	 *****************
var margin = {top: 20, right: 20, bottom: 20, left: 200},
	width = 1240 - margin.right - margin.left,
	height = 900 - margin.top - margin.bottom;
	
var i = 0,
	duration = 750,
	root;

var tree = d3.layout.tree()
	.size([height, width]);

var diagonal = d3.svg.diagonal()
	.projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("body").append("svg")
	.attr("width", width + margin.right + margin.left)
	.attr("height", height + margin.top + margin.bottom)
  .append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

root = treeData[0];
root.x0 = height / 2;
root.y0 = 0;
  
update(root);

d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
	  links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
	  .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
	  .attr("class", "node")
	  .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
	  .on("click", click);

  nodeEnter.append("circle")
	  .attr("r", 1e-6)
	  .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
	  .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
	  .attr("dy", ".35em")
	  .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
	  .text(function(d) { return d.name; })
	  .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
	  .duration(duration)
	  .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
	  .attr("r", 4.5)
	  .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
	  .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
	  .duration(duration)
	  .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
	  .remove();

  nodeExit.select("circle")
	  .attr("r", 1e-6);

  nodeExit.select("text")
	  .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
	  .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
	  .attr("class", "link")
	  .attr("d", function(d) {
		var o = {x: source.x0, y: source.y0};
		return diagonal({source: o, target: o});
	  });

  // Transition links to their new position.
  link.transition()
	  .duration(duration)
	  .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
	  .duration(duration)
	  .attr("d", function(d) {
		var o = {x: source.x, y: source.y};
		return diagonal({source: o, target: o});
	  })
	  .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
	d.x0 = d.x;
	d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
	d._children = d.children;
	d.children = null;
  } else {
  	window.location.href="career.php?select_name="+d.name;
	d.children = d._children;
	d._children = null;
  }
  update(d);
}

</script>
</body>
</html>
