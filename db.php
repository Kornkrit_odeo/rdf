<?php
define("RDFAPI_INCLUDE_DIR", "./api/");
include(RDFAPI_INCLUDE_DIR . "RdfAPI.php");
include(RDFAPI_INCLUDE_DIR . "rdql/RDQL.php");

class DB
{
	public static function query_base($id,$name,$domain,$is_name_to_key=false)
	{
		$sql = "SELECT ?obj, ?id, ?name
		FROM <datastore/new3.rdf>
		WHERE (?obj, <rdf:type>, ?s_y),
		(?obj, <ns0:".$name.">, ?name),
		(?obj, <ns0:".$id.">, ?id)

		AND ?s_y == '".$domain."'
		USING rdf FOR <http://www.w3.org/1999/02/22-rdf-syntax-ns#>,
		ns0 FOR <http://www.hozo.jp/owl/ICTCareer.owl#>,
		rdfs FOR <http://www.w3.org/2000/01/rdf-schema#>,
		owl FOR <http://www.w3.org/2002/07/owl#>";

		$result = array();
		if(preg_match('/^SELECT(.*) FROM/im',str_replace(array("\n","\t")," ", $sql),$matches))
		{
			if(isset($matches[1]))
			{
				$struck_key = array_map('trim',explode(',',$matches[1]));

				$parser = new RdqlParser();
				$parsed = & $parser->parseQuery($sql);
				$model = ModelFactory::getDefaultModel();
				$model->load($parsed['sources'][0]);
				$engine = new RdqlMemEngine();
				$queryResult = $engine->queryModel($model, $parsed, TRUE);
				foreach($queryResult as $items)
				{
					if(!isset($items['?obj']) || empty($items['?obj'])) continue;
					$obj	= ($is_name_to_key==TRUE) ? $items['?name']->label:$items['?obj']->uri;
					$result[$obj]['uri'] = base64_encode($obj);
					foreach($struck_key as $key)
					{
						if($key!='?obj')
						{
							$result[$obj][str_replace('?','',$key)] = $items[$key]->label;
						}
					}
				}
			}
		}
		return $result;
	}

	public static function query_career(&$return_data,$param=NULL)
	{
		$result = array();
		if(!empty($param))
		{
			$_w = '';
			if(isset($param['select_name']) && !empty($param['select_name']))
			{
				$_w .= " && ?name == '".$param['select_name']."'";
			}
			$sql = "SELECT ?obj, ?name ,?".$param['type']."
			FROM <datastore/new3.rdf>
			WHERE (?obj, <rdf:type>, ?z),
			(?obj, <ns0:".$param['name'].">, ?name),
			(?obj, <ns0:".$param['id'].">, ?".$param['type'].")

			AND ?z == '".$param['domain']."'".$_w." 

			USING rdf FOR <http://www.w3.org/1999/02/22-rdf-syntax-ns#>,
			ns0 FOR <http://www.hozo.jp/owl/ICTCareer.owl#>,
			rdfs FOR <http://www.w3.org/2000/01/rdf-schema#>,
			owl FOR <http://www.w3.org/2002/07/owl#>";

			$parser = new RdqlParser();
			$parsed = & $parser->parseQuery($sql);
			$model = ModelFactory::getDefaultModel();
			$model->load($parsed['sources'][0]);
			$engine = new RdqlMemEngine();
			$queryResult = $engine->queryModel($model, $parsed, TRUE);

			foreach($queryResult as $item)
			{
				if(!isset($item['?obj'])) continue;
				$obj = $item['?obj']->uri;
				if(!isset($return_data[$obj]))
				{
					$return_data[$obj] = array();
					$return_data[$obj]['name'] = $item['?name']->label;
				}
				if(!isset($return_data[$obj][$param['type']])) $return_data[$obj][$param['type']] = array();
				if(!in_array($item['?'.$param['type']]->uri, $return_data[$obj][$param['type']]))
				{
					$return_data[$obj][$param['type']][] = $param['base_data'][$item['?'.$param['type']]->uri];
				}
			}
		}
	}

	public static function query_prop(&$return_data,$param=NULL)
	{
		$result = array();
		if(!empty($param))
		{
			$_w = '';
			if(isset($param['uri']) && !empty($param['uri']))
			{
				$_w .= " && ?".$param['type']." == '".$param['uri']."'";
			}
			$sql = "SELECT ?obj, ?name ,?".$param['type']."
			FROM <datastore/new3.rdf>
			WHERE (?obj, <rdf:type>, ?z),
			(?obj, <ns0:".$param['name'].">, ?name),
			(?obj, <ns0:".$param['id'].">, ?".$param['type'].")

			AND ?z == '".$param['domain']."'".$_w." 

			USING rdf FOR <http://www.w3.org/1999/02/22-rdf-syntax-ns#>,
			ns0 FOR <http://www.hozo.jp/owl/ICTCareer.owl#>,
			rdfs FOR <http://www.w3.org/2000/01/rdf-schema#>,
			owl FOR <http://www.w3.org/2002/07/owl#>";
			$parser = new RdqlParser();
			$parsed = & $parser->parseQuery($sql);
			$model = ModelFactory::getDefaultModel();
			$model->load($parsed['sources'][0]);
			$engine = new RdqlMemEngine();
			$queryResult = $engine->queryModel($model, $parsed, TRUE);

			foreach($queryResult as $item)
			{
				if(!isset($item['?obj'])) continue;
				$obj = $item['?obj']->uri;
				if(!isset($return_data[$obj]))
				{
					$return_data[$obj] = array();
					$return_data[$obj]['name'] = $item['?name']->label;
				}
			}
		}
	}


	public static function query_group()
	{
		$sql = "SELECT ?obj, ?name
		FROM <datastore/new3.rdf>
		WHERE (?obj, <rdfs:subClassOf>, ?s_y),
		(?obj, <rdfs:label>, ?name)

		AND ?s_y == 'http://www.hozo.jp/owl/ICTCareer.owl#ICTCareerISCO';
		USING rdf FOR <http://www.w3.org/1999/02/22-rdf-syntax-ns#>,
		ns0 FOR <http://www.hozo.jp/owl/ICTCareer.owl#>,
		rdfs FOR <http://www.w3.org/2000/01/rdf-schema#>,
		owl FOR <http://www.w3.org/2002/07/owl#>";

		$result = array();
		if(preg_match('/^SELECT(.*) FROM/im',str_replace(array("\n","\t")," ", $sql),$matches))
		{
			if(isset($matches[1]))
			{
				$struck_key = array_map('trim',explode(',',$matches[1]));

				$parser = new RdqlParser();
				$parsed = & $parser->parseQuery($sql);
				$model = ModelFactory::getDefaultModel();
				$model->load($parsed['sources'][0]);
				$engine = new RdqlMemEngine();
				$queryResult = $engine->queryModel($model, $parsed, TRUE);
				$career_item = array();
				foreach($queryResult as $items)
				{
					if(!isset($items['?obj']) || empty($items['?obj'])) continue;
					$obj = $items['?obj']->uri;
					$list_in_group = self::query_base('has_ICTCareerID','has_CareerName',$obj,$is_name_to_key=true);
					if(!empty($list_in_group))
					{
						$career_item[$items['?name']->label] = $list_in_group;
					}
					else
					{
						$career_item[$items['?name']->label] = NULL;
					}


				}
			}
		}
		print_r($career_item);
		exit;
		return $career_item;
	}


/*
	public static function query_group($data_career_temp)
	{
		$return_data =array();
		$file_data = file_get_contents('datastore/new3.rdf');
		if(!empty($data_career_temp))
		{
			foreach($data_career_temp as $career_item)
			{
				$names = explode(' ',$career_item['name']);
				$names = array_map('ucfirst',$names);
				$names = implode('_',$names);
				$key = 'http://www.hozo.jp/owl/ICTCareer.owl#'.$names;

				if(stristr($file_data,$key))
				{
					$list_in_group = self::query_base('has_ICTCareerID','has_CareerName',$key,$is_name_to_key=true);
					if(!empty($list_in_group))
					{
						$career_item['list_in_group'] = $list_in_group;
					}
					else
					{
						$career_item['list_in_group'] = NULL;
					}
				}
				else
				{
					$career_item['list_in_group'] = NULL;
				}
				$return_data[] = $career_item;

			}
		}
		return $return_data;
	}
	*/
}