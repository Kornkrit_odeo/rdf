<?php 
class PathNew
{
	private $path_data;
	private $career_active;
	private $career_datas;
	private $group_data_level;
	private $group_sub_level_data;
	private $MY_SQL;

	public function __construct($career_active,$career_datas,$group_data_level,$group_sub_level_data)
	{
		$this->group_flow = array();
		$this->MY_SQL = new DBPDO();
		$this->path_data = array();
		$this->path_min_key = array();
		$this->career_active = $career_active;
		$this->career_datas = $career_datas;
		$this->group_data_level = $group_data_level;
		$this->min_value = 9999999;
		$this->min_path = array();
		$this->min_paths = array();
		$this->path_data[$career_active['id']] = array(
			'xid'=>$career_active['id'],
			'name'=>$career_active['name'],
			'sim'=>0,
			'min'=>0,
			'parent'=>NULL
		);
		$this->get_maps($this->career_active['id'],$this->path_data[$career_active['id']]);
	}

	private function get_maps($id,&$parent)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careerPathDefTB WHERE careerID1='".$id."' ORDER BY different ASC LIMIT 0,6 ");
		$this->search_map($maps,$parent);
	}

	private function search_map($maps,&$parent)
	{
		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
    			$_temp 	= $this->career_datas[$_m['careerID2']];
    			$min 	=  ($parent['min']+$_m['different']);
    			$children_data = array(
					'xid'=>$_temp['id'],
					'name'=>$_temp['name'],
					'sim'=>$_m['different'],
					'min'=>$min,
					'highlight'=>0,
					'parent_id'=>$parent['xid'],
					'parent'=>$parent['name']
				);
				$parent['children'][$_m['careerID2']] = $children_data;
				if($min<$this->min_value)
				{
					//$this->min_value 	= $min;
					//$this->min_path 	= $children_data;
				}
				$this->get_maps2($_m['careerID2'],$parent['children'][$_m['careerID2']]);
		    }
		}
	}

	private function get_maps2($id,&$parent)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careerPathDefTB WHERE careerID1='".$id."' ORDER BY different ASC LIMIT 0,6 ");
		$this->search_map2($maps,$parent);
	}

	private function search_map2($maps,&$parent)
	{
		if(!empty($maps))
		{
			$index = 0;
			foreach($maps as $_m)
    		{
    			$_temp 	= $this->career_datas[$_m['careerID2']];
    			$min 	=  ($parent['min']+$_m['different']);
    			$children_data = array(
					'xid'=>$_temp['id'],
					'name'=>$_temp['name'],
					'sim'=>$_m['different'],
					'min'=>$min,
					'highlight'=>0,
					'parent_id'=>$parent['xid'],
					'parent'=>$parent['name']
				);
				$parent['children'][$_m['careerID2']] = $children_data;
				if($min<$this->min_value && $_m['careerID2']==1330)
				{
					$this->min_value 	= $min;
					$this->min_path 	= $children_data;
				}
				$this->get_maps3($_m['careerID2'],$parent['children'][$_m['careerID2']]);
		    }
		}
	}

	private function get_maps3($id,&$parent)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careerPathDefTB WHERE careerID1='".$id."' AND careerID2='1330' ORDER BY different ASC LIMIT 0,6 ");
		$this->search_map3($maps,$parent);
	}

	private function search_map3($maps,&$parent)
	{
		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
    			$_temp 	= $this->career_datas[$_m['careerID2']];
    			$min 	=  ($parent['min']+$_m['different']);
    			$children_data = array(
					'xid'=>$_temp['id'],
					'name'=>$_temp['name'],
					'sim'=>$_m['different'],
					'min'=>$min,
					'highlight'=>0,
					'parent_id'=>$parent['xid'],
					'parent'=>$parent['name']
				);
				$parent['children'][$_m['careerID2']] = $children_data;
				if($min<$this->min_value)
				{
					$this->min_value 	= $min;
					$this->min_path 	= $children_data;
				}
		    }
		}
	}

	private function add_parent($children)
	{
		$_children = array();
		if(!empty($children))
		{
			foreach($children as $_c)
			{
				$key = md5($_c['xid'].$_c['sim'].$_c['min'].$_c['parent_id']);
				$highlight = 0;
				if(in_array($key, $this->path_min_key))
				{
					$highlight = 1;
				}
				$_children[] = array(
					'xid'=>$_c['xid'],
					'name'=>'('.$_c['xid'].')'.' '.$_c['name'],
					'sim'=>$_c['sim'],
					'min'=>$_c['min'],
					'highlight'=>$highlight,
					'parent'=>$_c['parent'],
					'children'=>(isset($_c['children'])) ? $this->add_parent($_c['children']):NULL
				);
			}
		}
		return $_children;
	}

	private function search_min_path($children,$for_check)
	{
		if(!empty($children) && isset($children['children']) && !empty($children['children']))
		{
			foreach($children['children'] as $id=>$_c)
			{
				if($id==$for_check['xid'] 
					&& $_c['sim']==$for_check['sim'] 
					&& $_c['min']==$for_check['min'] 
					&& $_c['parent_id']==$for_check['parent_id'])
				{
					$this->path_min_key[] = md5($id.$_c['sim'].$_c['min'].$_c['parent_id']);
					$this->search_min_path($this->path_data[$this->career_active['id']],$children);
				}
				else
				{
					$this->search_min_path($_c,$for_check);
				}
			}
		}
	}

	public function get_path()
	{
		$children = $this->path_data[$this->career_active['id']];
		$this->search_min_path($children,$this->min_path);
		$return_data = array();
		$root     = $children;

		$return_data[0] = array(
			'xid'=>$root['xid'],
			'name'=>$root['name'],
			'sim'=>$root['sim'],
			'parent'=>$root['parent'],
			'children'=>$this->add_parent($root['children'])
		);
		return array('min_value'=>$this->min_value,'min_pathname'=>$this->min_path,'data'=>$return_data);
	}
}

