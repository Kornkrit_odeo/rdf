<?php
include(dirname(__FILE__).'/config.php');
$career_id = isset($_GET['career_id']) ? $_GET['career_id']:'';
if(!is_numeric($career_id)){header('Location:input_career_position.php');}

$caceer_name = $MY_SQL->fetch("SELECT * FROM ictcareerisco WHERE ICTCareerID='$career_id' LIMIT 1");
$eduction = $MY_SQL->fetchAll("SELECT a.educateID,b.ICTFieldEducation as name,a.weight FROM educationWeight a LEFT JOIN icteducation b on a.educateID=b.ICTEducationID WHERE a.careerID='$career_id'");
$skill = $MY_SQL->fetchAll("SELECT a.skillID,b.skillName as name,a.weight FROM skillWeight a LEFT JOIN ictskillsfia b on a.skillID=b.ICTskillID WHERE a.careerID='$career_id'");
$softskill = $MY_SQL->fetchAll("SELECT a.softSkillID,b.SoftSkillName as name,a.weight FROM softSkillWeigth a LEFT JOIN softskill b on a.softSkillID=b.SoftskillID WHERE a.careerID='$career_id'");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ICT Career</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstap.css" rel="stylesheet">
    <script src="lib/d3.v3.min.js"></script>
    <script src="lib/d3.layout.cloud.js"></script>


    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>svg{border:1px solid #ccc;margin:0 auto;display:block;}</style>

</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <?php include 'nav_bar.php';?>


    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h2>Career Trend Of "<?php echo $caceer_name['CareerName'];?>"</h2>
        <hr />
   	</div>
    <div>
    	Eduction :<br/>
   		<div id="edutcation"></div>
    </div>
    <div>
    	Skill :<br/>
   		<div id="skill"></div>
    </div>
    <div>
    	Soft Skill :<br/>
   		<div id="softskill"></div>
    </div>
    <div style="clear:both;height:20px;width:100%;"></div>

        <?php if(!empty($eduction)):?>
        <script>(function() {
        	var _words = [];
        	<?php foreach($eduction as $_item):?>
        	_words.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
        	<?php endforeach;?>

			var fill = d3.scale.category20();

			var layout = d3.layout.cloud()
			    .size([400, 200])
			        .words(_words.map(function(d) {
      return {text: d.t, size: 10 + d.w, test: "haha"};
    }))			    .padding(5)
			    .rotate(function() { return ~~(Math.random() * 2) * 90; })
			    .font("Impact")
			    .fontSize(function(d) { return d.size; })
			    .on("end", draw);

			layout.start();

			function draw(words) {
			  d3.select("#edutcation").append("svg")
			      .attr("width", layout.size()[0])
			      .attr("height", layout.size()[1])
			    .append("g")
			      .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
			    .selectAll("text")
			      .data(words)
			    .enter().append("text")
			      .style("font-size", function(d) { return d.size + "px"; })
			      .style("font-family", "Impact")
			      .style("fill", function(d, i) { return fill(i); })
			      .attr("text-anchor", "middle")
			      .attr("transform", function(d) {
			        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			      })
			      .text(function(d) { return d.text; });
			}

			})();</script>

		<?php endif;?>


		<?php if(!empty($skill)):?>
        <script>(function() {
        	var _words = [];
        	<?php foreach($skill as $_item):?>
        	_words.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
        	<?php endforeach;?>

			var fill = d3.scale.category20();

			var layout = d3.layout.cloud()
			    .size([400, 200])
			        .words(_words.map(function(d) {
      return {text: d.t, size: 10 + d.w, test: "haha"};
    }))			    .padding(5)
			    .rotate(function() { return ~~(Math.random() * 2) * 90; })
			    .font("Impact")
			    .fontSize(function(d) { return d.size; })
			    .on("end", draw);

			layout.start();

			function draw(words) {
			  d3.select("#skill").append("svg")
			      .attr("width", layout.size()[0])
			      .attr("height", layout.size()[1])
			    .append("g")
			      .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
			    .selectAll("text")
			      .data(words)
			    .enter().append("text")
			      .style("font-size", function(d) { return d.size + "px"; })
			      .style("font-family", "Impact")
			      .style("fill", function(d, i) { return fill(i); })
			      .attr("text-anchor", "middle")
			      .attr("transform", function(d) {
			        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			      })
			      .text(function(d) { return d.text; });
			}

			})();</script>

		<?php endif;?>


		<?php if(!empty($softskill)):?>
        <script>(function() {
        	var _words = [];
        	<?php foreach($softskill as $_item):?>
        	_words.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
        	<?php endforeach;?>

			var fill = d3.scale.category20();

			var layout = d3.layout.cloud()
			    .size([400, 200])
			        .words(_words.map(function(d) {
      return {text: d.t, size: 10 + d.w, test: "haha"};
    }))			    .padding(5)
			    .rotate(function() { return ~~(Math.random() * 2) * 90; })
			    .font("Impact")
			    .fontSize(function(d) { return d.size; })
			    .on("end", draw);

			layout.start();

			function draw(words) {
			  d3.select("#softskill").append("svg")
			      .attr("width", layout.size()[0])
			      .attr("height", layout.size()[1])
			    .append("g")
			      .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
			    .selectAll("text")
			      .data(words)
			    .enter().append("text")
			      .style("font-size", function(d) { return d.size + "px"; })
			      .style("font-family", "Impact")
			      .style("fill", function(d, i) { return fill(i); })
			      .attr("text-anchor", "middle")
			      .attr("transform", function(d) {
			        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			      })
			      .text(function(d) { return d.text; });
			}

			})();</script>

		<?php endif;?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>