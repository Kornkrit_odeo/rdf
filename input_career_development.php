<?php
include(dirname(__FILE__).'/config.php');

$datas = $MY_SQL->fetchAll("Select idUnitGroup as id , groupName as name from careerGroupTb");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ICT Career</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <?php include 'nav_bar.php';?>


    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h2>Career Development</h2>
        <hr />
        <form class="form-inline" method="get" action="development2.php">
            <div class="form-group">
                <label for="exampleInputName2"> Select compare position : </label>
                <br />
                <select required class="form-control" name="select_name1">
                    <option value="">Select Jobs</option>
                    <?php
                    foreach($datas as $data)
                    {
                        echo "<option value=".$data['id'].">".$data['name']."</option>";
                    }
                    ?>
                </select>
                <label for="exampleInputName2"> with : </label>
                <select required class="form-control" name="select_name2">
                    <option value="">Select Jobs</option>
                    <?php
                    foreach($datas as $data)
                    {
                        echo "<option value=".$data['id'].">".$data['name']."</option>";
                    }
                    ?>
                </select>
            </div>
            <br /><br />
            <div class="form-group">
                <input type="submit" class="btn btn-lg btn-primary" value="Compare">
            </div>
        </form>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>
