<?php
include(dirname(__FILE__).'/config.php');

$name1 = $_GET['select_name1'];
$name2 = $_GET['select_name2'];

$name_name1 = $MY_SQL->fetchAll("Select idUnitGroup as id , groupName as name from careerGroupTb where idUnitGroup = $name1");
$name_name2 = $MY_SQL->fetchAll("Select idUnitGroup as id , groupName as name from careerGroupTb where idUnitGroup = $name2");


$datas1 = $MY_SQL->fetchAll("Select * from careerhasskillsample a  where careerID = $name1");
$datas2 = $MY_SQL->fetchAll("Select * from careerhasskillsample a  where careerID = $name2");


$dataAll = array();
$orderSkill = array();
$current = '[';
$target = '[';
$check = 0;
$i = 0;

foreach($datas2 as $skill) {
    $datas2[$i]['order'] = 10 + $skill['highLevel'];
    foreach($datas1 as $skill1) {
        if($skill['SkillID'] == $skill1['SkillID']) {
//            var_dump($skill);
//            var_dump($skill1);

            $datas2[$i]['order'] = 10 + ($skill['highLevel'] - $skill1['highLevel']);

        }
    }

//    var_dump($datas2[$i]);

    $i++;
}

//exit;
function cmp($a, $b)
{
    if ($a["order"] == $b["order"])
    {
        return 0;
    }
    return ($a["order"] < $b["order"]) ? -1 : 1;

}

usort($datas2, "cmp");

//var_dump($datas2); exit;
//SELECT * FROM `careerhasskillsample` WHERE `careerID` in (2511 , 2514) and `SkillID` = 'SUAS'
foreach($datas2 as $skill) {
    $check = 0;
    $target .= '{ y :['. $skill['lowLevel'].','.$skill['highLevel'].'] , label:'.'"'.$skill['SkillID'].'" } ,';
    foreach($datas1 as $skill1){
        if($skill['SkillID'] == $skill1['SkillID']) {
            $check = 1;
            $current .= '{ y :[' . $skill1['lowLevel'] . ',' . $skill1['highLevel'] . '] , label:' . '"' . $skill1['SkillID'] . '" } ,';
        }
    }
    if($check == 0){
        $current .= '{ y :[' . 0 . ',' . 0 . '] , label:' . '"' . $skill1['SkillID'] . '" } ,';
    }
}

//var_dump($target); exit;

$target .= ']';
$current .= ']';

?>

<!DOCTYPE HTML>
<html>

<head>
    <script type="text/javascript">
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer",
                {
                    title:{
                        text: "Comparison of Two Positions",
                    },
                    exportEnabled: true,
                    axisY: {
                        includeZero:false,
                        title: "Level"
//                        suffix: "°C",
                    },
                    axisX:{
                        title: "Skill"

//                        labelFormatter: function(e){
//                            return  e.value;
//                        }
                    },
                    toolTip: {
                        shared: true,
                    },
                    data: [
                        {
                            type: "rangeColumn",
//                            yValueFormatString: "#0.## °C",
                            name: "Current : <?php echo $name_name1[0]['name']; ?>",
                            showInLegend: true,
                            dataPoints: <?php echo $current; ?>
                        },
                        {
                            type: "rangeColumn",
                            showInLegend: true,
                            name: "Target :<?php echo $name_name2[0]['name']; ?>",
//                            yValueFormatString: "#0.## °C",
                            dataPoints: <?php echo $target; ?>

                        }
                    ]
                });
            chart.render();
        }
    </script>
    <script type="text/javascript" src="js/canvasjs.min.js"></script>
</head>
<body>
<div id="chartContainer" style="width:2000px"></div>
</body>


</html>
