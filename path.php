<?php 
class Path
{
	private $path_data;
	private $career_active;
	private $career_datas;
	private $group_data_level;
	private $group_sub_level_data;
	private $MY_SQL;
	private $group_flow;

	public function __construct($career_active,$career_datas,$group_data_level,$group_sub_level_data)
	{
		$this->group_flow = array();
		$this->group_flow[3512] = array(3514,2511,2512,2513,2514,2519);
		$this->group_flow[3514] = array(3512,2511,2512,2513,2514,2519);

		$this->group_flow[2511] = array(2512,2513,2514,2519,1330);
		$this->group_flow[2512] = array(2511,2513,2514,2519,1330);
		$this->group_flow[2513] = array(2511,2512,2514,2519,1330);
		$this->group_flow[2514] = array(2511,2512,2513,2519,1330);
		$this->group_flow[2519] = array(2511,2512,2513,2514,1330);

		$this->group_flow[3511] = array(3513,2521,2522,2523,2529);
		$this->group_flow[3513] = array(3511,2521,2522,2523,2529);

		$this->group_flow[2521] = array(2522,2523,2529,1330);
		$this->group_flow[2522] = array(2521,2523,2529,1330);
		$this->group_flow[2523] = array(2521,2522,2529,1330);
		$this->group_flow[2529] = array(2521,2522,2523,1330);

		$this->MY_SQL = new DBPDO();
		$this->path_data = array();
		$this->career_active = $career_active;
		$this->career_datas = $career_datas;
		$this->group_data_level = $group_data_level;
		$this->group_sub_level_data = $group_sub_level_data;
		$this->get_maps($this->career_active);
	}

	private function get_maps($career_data)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC");
		//echo "SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC";
		//exit;
		$this->search_map($maps,$career_data);
	}

	private function search_map($maps,$career_data)
	{
		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
    			//if($_m['careerid1']==1330){$_m['careerid1']=1331;}
				$g_items = isset($this->group_sub_level_data[$_m['careerid1']]) ? $this->group_sub_level_data[$_m['careerid1']]:NULL;
				if(!empty($g_items))
				{
					$this->path_data[$career_data['id']]['data'] = $career_data;
					foreach($g_items as $sub_group=>$g_item)
					{
						$group_flows = isset($this->group_flow[$career_data['sub_group']]) ? $this->group_flow[$career_data['sub_group']]:NULL;
				        if(!empty($g_item) && $g_item['level']==$career_data['level'] && !empty($group_flows) && in_array($g_item['sub_group'],$group_flows))
				        {
				        	$g_item['sim'] = floatval($_m['sim']);
				            $this->path_data[$career_data['id']]['ole_level'][$g_item['id']] = $g_item;
				            $this->get_maps2($g_item);
				        }
				        else if(!empty($g_item) && $g_item['level'] > $career_data['level'] && (!empty($group_flows) && in_array($g_item['sub_group'],$group_flows) || $g_item['level'] ==4))
				        {
				        	$g_item['sim'] = floatval($_m['sim']);
				            $this->path_data[$career_data['id']]['up_level'][$g_item['id']] = $g_item;
				            $this->get_maps($g_item);
				        }
				    }
				}
		    }
		}
	}

	private function get_maps2($career_data)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC");
		//echo "SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC";
		//exit;
		$this->search_map2($maps,$career_data);
	}

	private function search_map2($maps,$career_data)
	{
		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
    			//if($_m['careerid1']==1330){$_m['careerid1']=1331;}
				$g_items = isset($this->group_sub_level_data[$_m['careerid1']]) ? $this->group_sub_level_data[$_m['careerid1']]:NULL;
				if(!empty($g_items))
				{
					$this->path_data[$career_data['id']]['data'] = $career_data;
					foreach($g_items as $sub_group=>$g_item)
					{
						$group_flows = isset($this->group_flow[$career_data['sub_group']]) ? $this->group_flow[$career_data['sub_group']]:NULL;
				        if(!empty($g_item) && $g_item['level']==$career_data['level'] && $g_item['level']==1 && !empty($group_flows) && in_array($g_item['sub_group'],$group_flows))
				        {
				        	$g_item['sim'] = floatval($_m['sim']);
				            $this->path_data[$career_data['id']]['ole_level'][$g_item['id']] = $g_item;
				            //$this->get_maps2($g_item);
				        }
				        else if(!empty($g_item) && $g_item['level'] > $career_data['level'] && (!empty($group_flows) && in_array($g_item['sub_group'],$group_flows) || $g_item['level'] ==4))
				        {
				        	$g_item['sim'] = floatval($_m['sim']);
				            $this->path_data[$career_data['id']]['up_level'][$g_item['id']] = $g_item;
				            //$this->get_maps($g_item);
				        }
				    }
				}
		    }
		}
	}

	private function get_maps3($career_data)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC");
		$this->search_map3($maps,$career_data);
	}

	private function search_map3($maps,$career_data)
	{

		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
				$g_item = isset($this->career_datas[$_m['careerid1']]) ? $this->career_datas[$_m['careerid1']]:NULL;
				$this->path_data[$career_data['id']]['data'] = $career_data;

		        if(!empty($g_item) && $g_item['level']==$career_data['level'] && $g_item['sub_group']==$career_data['sub_group'])
		        {
		        	$g_item['sim'] = $_m['sim'];
		            $this->path_data[$career_data['id']]['ole_level'][$g_item['id']] = $g_item;
		            $this->get_maps4($g_item);
		        }
		        else if(!empty($g_item) && $g_item['level'] > $career_data['level'])
		        {
		        	$g_item['sim'] = $_m['sim'];
		            $this->path_data[$career_data['id']]['up_level'][$g_item['id']] = $g_item;
		            ///$this->get_maps4($g_item);
		        }
		    }
		}
	}

	private function get_maps4($career_data)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC");
		$this->search_map4($maps,$career_data);
	}

	private function search_map4($maps,$career_data)
	{

		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
				$g_item = isset($this->career_datas[$_m['careerid1']]) ? $this->career_datas[$_m['careerid1']]:NULL;
				$this->path_data[$career_data['id']]['data'] = $career_data;

		        if(!empty($g_item) && $g_item['level']==$career_data['level'] && $g_item['sub_group']==$career_data['sub_group'])
		        {
		        	$g_item['sim'] = $_m['sim'];
		            $this->path_data[$career_data['id']]['ole_level'][$g_item['id']] = $g_item;
		            $this->get_maps5($g_item);
		        }
		        else if(!empty($g_item) && $g_item['level'] > $career_data['level'])
		        {
		        	$g_item['sim'] = $_m['sim'];
		            $this->path_data[$career_data['id']]['up_level'][$g_item['id']] = $g_item;
		            //$this->get_maps5($g_item);
		        }
		    }
		}
	}

	private function get_maps5($career_data)
	{
		$maps = $this->MY_SQL->fetchAll("SELECT * FROM careertocareer WHERE careerid2='".$career_data['sub_group']."' ORDER BY sim DESC");
		$this->search_map5($maps,$career_data);
	}

	private function search_map5($maps,$career_data)
	{

		if(!empty($maps))
		{
			foreach($maps as $_m)
    		{
				$g_item = isset($this->career_datas[$_m['careerid1']]) ? $this->career_datas[$_m['careerid1']]:NULL;
				$this->path_data[$career_data['id']]['data'] = $career_data;

		        if(!empty($g_item) && $g_item['level']==$career_data['level'])
		        {
		        	$g_item['sim'] = $_m['sim'];
		            $this->path_data[$career_data['id']]['ole_level'][$g_item['id']] = $g_item;
		            //$this->get_maps5($g_item);
		        }
		        else if(!empty($g_item) && $g_item['level'] > $career_data['level'])
		        {
		        	$g_item['sim'] = $_m['sim'];
		            $this->path_data[$career_data['id']]['up_level'][$g_item['id']] = $g_item;
		            //$this->get_maps5($g_item);
		        }
		    }
		}
	}

	private function get_child($career_id)
	{
		$children = array();
		if(isset($this->path_data[$career_id]) && !empty($this->path_data[$career_id]))
		{
			$current_career = $this->path_data[$career_id];

			if(isset($current_career['ole_level']) && !empty($current_career['ole_level']))
			{
				foreach($current_career['ole_level'] as $_ole_level)
				{
					$children[] = array(
						'xid'=>$_ole_level['id'],
						'name'=>$_ole_level['name'],
						'sim'=>$_ole_level['sim'],
						'level'=>$_ole_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child2($_ole_level['id'])
					);
				}
			}

			if(isset($current_career['up_level']) && !empty($current_career['up_level']))
			{
				foreach($current_career['up_level'] as $_up_level)
				{
					$children[] = array(
						'xid'=>$_up_level['id'],
						'name'=>$_up_level['name'],
						'sim'=>$_up_level['sim'],
						'level'=>$_up_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child2($_up_level['id'])
					);
				}
			}
		}
		return $children;
	}

	private function get_child2($career_id)
	{
		$children = array();
		if(isset($this->path_data[$career_id]) && !empty($this->path_data[$career_id]))
		{
			$current_career = $this->path_data[$career_id];

			if(isset($current_career['ole_level']) && !empty($current_career['ole_level']))
			{
				foreach($current_career['ole_level'] as $_ole_level)
				{
					$children[] = array(
						'xid'=>$_ole_level['id'],
						'name'=>$_ole_level['name'],
						'sim'=>$_ole_level['sim'],
						'level'=>$_ole_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child3($_ole_level['id'])
					);
				}
			}

			if(isset($current_career['up_level']) && !empty($current_career['up_level']))
			{
				foreach($current_career['up_level'] as $_up_level)
				{
					$children[] = array(
						'xid'=>$_up_level['id'],
						'name'=>$_up_level['name'],
						'sim'=>$_up_level['sim'],
						'level'=>$_up_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child3($_up_level['id'])
					);
				}
			}
		}
		return $children;
	}

	private function get_child3($career_id)
	{
		$children = array();
		if(isset($this->path_data[$career_id]) && !empty($this->path_data[$career_id]))
		{
			$current_career = $this->path_data[$career_id];

			if(isset($current_career['ole_level']) && !empty($current_career['ole_level']))
			{
				foreach($current_career['ole_level'] as $_ole_level)
				{
					$children[] = array(
						'xid'=>$_ole_level['id'],
						'name'=>$_ole_level['name'],
						'sim'=>$_ole_level['sim'],
						'level'=>$_ole_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child4($_ole_level['id'])
					);
				}
			}

			if(isset($current_career['up_level']) && !empty($current_career['up_level']))
			{
				foreach($current_career['up_level'] as $_up_level)
				{
					$children[] = array(
						'xid'=>$_up_level['id'],
						'name'=>$_up_level['name'],
						'sim'=>$_up_level['sim'],
						'level'=>$_up_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child4($_up_level['id'])
					);
				}
			}
		}
		return $children;
	}

	private function get_child4($career_id)
	{
		$children = array();
		if(isset($this->path_data[$career_id]) && !empty($this->path_data[$career_id]))
		{
			$current_career = $this->path_data[$career_id];

			if(isset($current_career['ole_level']) && !empty($current_career['ole_level']))
			{
				foreach($current_career['ole_level'] as $_ole_level)
				{
					$children[] = array(
						'xid'=>$_ole_level['id'],
						'name'=>$_ole_level['name'],
						'sim'=>$_ole_level['sim'],
						'level'=>$_ole_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child5($_ole_level['id'])
					);
				}
			}

			if(isset($current_career['up_level']) && !empty($current_career['up_level']))
			{
				foreach($current_career['up_level'] as $_up_level)
				{
					$children[] = array(
						'xid'=>$_up_level['id'],
						'name'=>$_up_level['name'],
						'sim'=>$_up_level['sim'],
						'level'=>$_up_level['level'],
						'parent'=>$current_career['data']['name'],
						'children'=>$this->get_child5($_up_level['id'])
					);
				}
			}
		}
		return $children;
	}

	private function get_child5($career_id)
	{
		$children = array();
		if(isset($this->path_data[$career_id]) && !empty($this->path_data[$career_id]))
		{
			$current_career = $this->path_data[$career_id];

			if(isset($current_career['ole_level']) && !empty($current_career['ole_level']))
			{
				foreach($current_career['ole_level'] as $_ole_level)
				{
					$children[] = array(
						'xid'=>$_ole_level['id'],
						'name'=>$_ole_level['name'],
						'sim'=>$_ole_level['sim'],
						'level'=>$_ole_level['level'],
						'parent'=>$current_career['data']['name'],
						/*'children'=>$this->get_child($_ole_level['id'])*/
					);
				}
			}

			if(isset($current_career['up_level']) && !empty($current_career['up_level']))
			{
				foreach($current_career['up_level'] as $_up_level)
				{
					$children[] = array(
						'xid'=>$_up_level['id'],
						'name'=>$_up_level['name'],
						'sim'=>$_up_level['sim'],
						'level'=>$_up_level['level'],
						'parent'=>$current_career['data']['name'],
						/*'children'=>$this->get_child($_up_level['id'])*/
					);
				}
			}
		}
		return $children;
	}

	public function get_path()
	{
		//print_r($this->career_active);
		//exit;
		$return[0] = array(
			'xid'=>$this->career_active['id'],
			'name'=>$this->career_active['name'],
			'level'=>$this->career_active['level'],
			'sim'=>0,
			'parent'=>NULL,
			'children'=>$this->get_child($this->career_active['id'])
		);
		//print_r($return);
		//exit;
		if(!empty($return[0]['children']))
		{
			foreach($return[0]['children'] as &$c1)
			{
				$key = md5(microtime());
				$sim = 0;
				$c1['total'] = $sim+=$c1['sim'];
				$c1['mykey'] = $key;
				if(isset($c1['children']) && !empty($c1['children']))
				{
					foreach($c1['children'] as &$c2)
					{
						$c2['total'] = $c1['total']+=$c2['sim'];
						$c2['mykey'] = $key;
						if(isset($c2['children']) && !empty($c2['children']))
						{
							foreach($c2['children'] as &$c3)
							{
								$c3['total'] = $c2['total']+=$c3['sim'];
								$c3['mykey'] = $key;
							}
						}
					}
				}
			}
		}
		$max = 0;
		$max_key = '';
		if(!empty($return[0]['children']))
		{
			foreach($return[0]['children'] as $c1)
			{
				if($c1['total'] > $max)
				{
					$max 		= $c1['total'];
					$max_key 	= $c1['mykey'];
				}
			}
		}
		return array('max_key'=>$max_key,'data'=>$return);
	}
}