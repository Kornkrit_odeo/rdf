<?php
include(dirname(__FILE__).'/config.php');
include(dirname(__FILE__).'/pathnew.php');


/* Career */
$career_id = isset($_GET['career_id']) ? $_GET['career_id']:'';

$counties = $MY_SQL->fetchAll("SELECT a.* FROM `ictcareerisco` a WHERE a.`ICTCareerID` NOT IN('2519','2529')");
$groups   = $MY_SQL->fetchAll('SELECT * FROM `careerGroupTb`');

$grop_level_data = array();
$group_sub_level_data = array();
$sort_data_career = array();


$group_level = array();
$group_level['351']=1;
$group_level['251']=2;
$group_level['252']=3;
$group_level['133']=4;

foreach($counties as $item) 
{
    $_level='';
    $_sub_group ='';
    foreach($group_level as $_le=>$level)
    {
        if(preg_match('/^'.$_le.'.*/',$item['ICTCareerID']))
        {
            $grop_level_data[$level][] = array('id'=>$item['ICTCareerID'],'name'=>$item['CareerName']);
            $_level = $level; break;
        }
    }
    foreach($groups as $_g)
    {
        //if($_g['idUnitGroup']=='1330'){$_g['idUnitGroup']='1331';}
        if(preg_match('/^'.$_g['idUnitGroup'].'.*/',$item['ICTCareerID']) || $item['ICTCareerID']==$_g['idUnitGroup'])
        {
            $group_sub_level_data[$_g['idUnitGroup']][] = array('id'=>$item['ICTCareerID'],'name'=>$item['CareerName'],'level'=>$_level,'sub_group'=>$_g['idUnitGroup']);
            $_sub_group = $_g['idUnitGroup'];
            break;
        }
    }
    $sort_data_career[$item['ICTCareerID']] = array('id'=>$item['ICTCareerID'],'name'=>$item['CareerName'],'level'=>$_level,'sub_group'=>$_sub_group);
}
asort($sort_data_career);
//print_r($group_sub_level_data);exit;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ICT Career</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstap.css" rel="stylesheet">
    <link href="index.css" rel="stylesheet" type="text/css" />

   

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <?php include 'nav_bar.php';?>


    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h2>Career Path</h2>
        <hr />
        <form class="form-inline" method="get" action="input_career_path.php">
            <div class="form-group">
                <label for="exampleInputName2"> Select position : </label>
                <select required class="form-control" name="career_id">
                    <option value="">Select Jobs</option>
                    <?php
                    foreach($sort_data_career as $item => $key)
                    {
                        if($key['level']!=4)
                        {
                            $se = ($item==$career_id) ? ' selected ':'';
                            echo "<option ".$se." value=".$item.">".$key['name']."</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="submit" class="btn btn-primary" value="Search" >
        </form>

    </div> <!-- /container -->
<?php
/*
133 Service manager
251 develop group
252 admin group
351 technician
*/

if(is_numeric($career_id))
{
    $career_active = isset($sort_data_career[$career_id]) ? $sort_data_career[$career_id]:NULL;
    if(empty($career_active)){header('Location:input_career_path.php');}
    $path = new PathNew($career_active,$sort_data_career,$grop_level_data,$group_sub_level_data);
    $data_result = $path->get_path();
    //print_r($data_result);
    //exit;
}
?>
<script src="lib/d3.v3.min.js"></script>
<script src="lib/d3.layout.cloud.js"></script>
<script src="lib/d3.tip.v0.6.3.js"></script>
<script>
var treeData = <?php echo json_encode($data_result['data']);?>;

// ************** Generate the tree diagram  *****************
var margin = {top: 20, right: 20, bottom: 20, left: 200},
    width = 1200 - margin.right - margin.left,
    height = 500 - margin.top - margin.bottom;
    
var i = 0,
    duration = 350,
    root;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var tip = d3.tip()
  .attr('class', 'd3-tip')
  .offset([-10, 0])
  .html(function(d) {
    return "<span style='color:red'>" + d.sim + "</span>";
  })
svg.call(tip);


root = treeData[0];
root.x0 = height / 2;
root.y0 = 0;
  
update(root);

d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .attr("class", function(d) {
        return (d.highlight==1) ? 'node-active':'node';
      })
      .on("click", click);

  nodeEnter.append("circle")
      .attr("r", 1e-6)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
      .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
      .text(function(d) { return d.name; })
      .on('mouseover',tip.show)
      .on('mouseout',tip.hide)
      .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("circle")
      .attr("r", 4.5)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("circle")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("fill", "none")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      })
      .attr("class", function(d) {
        return (d.target.highlight==1) ? 'active':'link';
      });
  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}
</script>
 
</body>
</html>
