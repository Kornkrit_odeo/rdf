<?php
include(dirname(__FILE__).'/config.php');

/* Career */
$counties = $MY_SQL->fetchAll("SELECT * FROM ictcareerisco");
$sort_data_career = array();
foreach($counties as $item) {

    $sort_data_career[$item['ICTCareerID']] = $item['CareerName'];
} 
$career_id = isset($_GET['career_id']) ? $_GET['career_id']:'';


asort($sort_data_career);

$skill_all = $MY_SQL->fetchAll("SELECT a.weight FROM ictSkillweight");

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ICT Career</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="lib/d3.v3.min.js"></script>
    <script src="lib/d3.layout.cloud.js"></script>
    <style>svg{border:1px solid #ccc;margin:0 auto;display:block;}p{font-weight:bold;}</style>

</head>

<body>

<div class="container">

    <!-- Static navbar -->
    <?php include 'nav_bar.php';?>
<?php
    $caceer_name = '';
    if(is_numeric($career_id)) 
    {
        $caceer_name = $MY_SQL->fetch("SELECT * FROM ictcareerisco WHERE ICTCareerID='$career_id' LIMIT 1");
        $caceer_name = $caceer_name['CareerName'];
    }
    ?>

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h2>Career Trend <?php echo ($caceer_name!='') ? '"'.$caceer_name.'"':'';?></h2>
        <hr />
        <form class="form-inline" method="get" action="input_career_position.php">
            <div class="form-group">
                <label for="exampleInputName2"> Select position : </label>
                <select required class="form-control" name="career_id">
                    <option value="">Select Jobs</option>
                    <?php
                    foreach($sort_data_career as $item => $key)
                    {
                        $se = ($item==$career_id) ? ' selected ':'';
                        $value_key = str_replace(' ', '_', $key);
                        echo "<option ".$se." value=".$item.">".$key."</option>";
                    }
                    ?>
                </select>
            </div>
            <input type="submit" class="btn btn-primary" value="Search" >
        </form>

    </div> <!-- /container -->

    <?php
    if(is_numeric($career_id)) 
    {
        $eduction = $MY_SQL->fetchAll("SELECT a.educateID,b.ICTFieldEducation as name,a.weight FROM educationWeight a LEFT JOIN icteducation b on a.educateID=b.ICTEducationID WHERE a.careerID='$career_id'");
        $skill = $MY_SQL->fetchAll("SELECT a.skillID,b.skillName as name,a.weight FROM skillWeight a LEFT JOIN ictskillsfia b on a.skillID=b.ICTskillID WHERE a.careerID='$career_id'");
        $softskill = $MY_SQL->fetchAll("SELECT a.softSkillID,b.SoftSkillName as name,a.weight FROM softSkillWeigth a LEFT JOIN softskill b on a.softSkillID=b.SoftskillID WHERE a.careerID='$career_id'");
        $skill_all = $MY_SQL->fetchAll("SELECT a.skillID,b.skillName as name,a.weight FROM ictSkillweight a LEFT JOIN ictskillsfia b on a.skillID=b.ICTskillID");

    ?>
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4">
        <p>Eduction :</p>
        <div id="edutcation"></div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
       <p>Skill :</p>
        <div id="skill"></div>
      </div>     
      <div class="col-lg-4 col-md-4 col-sm-4">
      <p>Soft Skill :</p>
        <div id="softskill"></div>
      </div>
    </div><br/>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <p style="text-align:center;">Skill All :</p>
        <div id="skill_all"></div>
      </div>
    </div>
      <?php if(!empty($skill_all)):?>
        <script>(function() {
            var _wordsx = [];
            <?php foreach($skill_all as $_item):?>
            _wordsx.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
            <?php endforeach;?>

            var fill = d3.scale.category20();

            var layout = d3.layout.cloud()
                .size([620, 400])
                    .words(_wordsx.map(function(d) {
      return {text: d.t, size: 10 + d.w};
    }))             .padding(5)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", draw);

            layout.start();

            function draw(words) {
              d3.select("#skill_all").append("svg")
                  .attr("width", layout.size()[0])
                  .attr("height", layout.size()[1])
                .append("g")
                  .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }

            })();</script>

        <?php endif;?>

        <?php if(!empty($eduction)):?>
        <script>(function() {
            var _words1 = [];
            <?php foreach($eduction as $_item):?>
            _words1.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
            <?php endforeach;?>

            var fill = d3.scale.category20();

            var layout = d3.layout.cloud()
                .size([320, 200])
                    .words(_words1.map(function(d) {
      return {text: d.t, size: 10 + d.w};
    }))             .padding(5)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", draw);

            layout.start();

            function draw(words) {
              d3.select("#edutcation").append("svg")
                  .attr("width", layout.size()[0])
                  .attr("height", layout.size()[1])
                .append("g")
                  .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }

            })();</script>

        <?php endif;?>


        <?php if(!empty($skill)):?>
        <script>(function() {
            var _words2 = [];
            <?php foreach($skill as $_item):?>
            _words2.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
            <?php endforeach;?>

            var fill = d3.scale.category20();

            var layout = d3.layout.cloud()
                .size([320, 200])
                    .words(_words2.map(function(d) {
      return {text: d.t, size: 10 + d.w};
    }))             .padding(5)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", draw);

            layout.start();

            function draw(words) {
              d3.select("#skill").append("svg")
                  .attr("width", layout.size()[0])
                  .attr("height", layout.size()[1])
                .append("g")
                  .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }

            })();</script>

        <?php endif;?>


        <?php if(!empty($softskill)):?>
        <script>(function() {
            var _words3 = [];
            <?php foreach($softskill as $_item):?>
            _words3.push({t:'<?php echo $_item['name'];?>',w:<?php echo $_item['weight'];?>*90});
            <?php endforeach;?>

            var fill = d3.scale.category20();

            var layout = d3.layout.cloud()
                .size([320, 200])
                    .words(_words3.map(function(d) {
      return {text: d.t, size: 10 + d.w};
    }))             .padding(5)
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", draw);

            layout.start();

            function draw(words) {
              d3.select("#softskill").append("svg")
                  .attr("width", layout.size()[0])
                  .attr("height", layout.size()[1])
                .append("g")
                  .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }

            })();</script>

        <?php endif;?>

        <?php }?>
    <div style="clear:both;height:20px;width:100%;"></div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstap.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
</body>
</html>
